import csv
from scipy.io import loadmat
import h5py


def read_mat_robust(filename):
    try:
        data = loadmat(filename)
    except:
        data = h5py.File(filename)
    return data


def read_class_groups(csv_file):
    with open(csv_file, 'r') as f:
        my_read = (csv.reader(f))
        dict = {}
        for i in my_read:
            if len(i) == 2:
                dict[i[0]] = i[1]
            else:
                dict[i[0]] = i[1] + ',' + i[2]
    print(dict)
    return dict

if __name__ == "__main__":
    read_class_groups('class_groups.csv')
